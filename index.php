<?php
/**
 * Plugin Name:       Onepager - Innovent Basic Blocks Plugin
 * Plugin URI:        http://innovent-finland.com/onepager-plugin/#onepager-basic
 * Description:       Onepager Innovent Basic Blocks Plugin: Basic bootstrap blocks from original onepager Plugin.
 * Version:           1.0.0
 * Author:            Innovent Finland
 * Author URI:        http://www.innovent-finland.com
 * License:           GPL-3.0+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       onepager-innovent-basic
 * Domain Path:       /languages
 * Bitbucket Plugin URI: jonashergenhahn/innovent-onepager-basic
 * Bitbucket Branch:  master
 */

add_action('onepager_loaded', function(){

	/**
	*	Load Blocks
	**/
	$blocks_path = __DIR__ . "/blocks";
	$blocks_url = plugins_url('blocks', __FILE__);
	$blocks_groups = ['Basic Blocks (Plugin)'];

	onepager()->blockManager()->loadAllFromPath($blocks_path, $blocks_url, $blocks_groups);

});

add_action( 'wp_enqueue_scripts', 'innovent_onepager_basic_scripts_enqueue' );

function innovent_onepager_basic_scripts_enqueue() {
    wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), NULL, true );
    wp_register_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, NULL, 'all' );
 
    wp_enqueue_script( 'bootstrap' );
    wp_enqueue_style( 'bootstrap' );
}