=== Onepager - Innovent Blocks Plugin ===
Contributors: jonashergenhahn
Tags: page builder, builder, onepage builder, drag and drop, reactjs, bootstrap, fontAwesome, gulp, less, layout, grid, composer, webpack, bower, ui
Stable tag: 1.0.0
Requires at least: 4.2
Tested up to: 4.7.2
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Onepager Innovent Basic Blocks Plugin: Basic bootstrap blocks from original onepager Plugin.

== Requirements ==

 * PHP 5.5+

== Installation ==

Installing the plugins is just like installing other WordPress plugins.